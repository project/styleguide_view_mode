<?php

/**
 * @file
 * Administrative page callbacks for the Style guide view mode module.
 */

/**
 * Form constructor for style guide view mode settings.
 */
function styleguide_view_mode_settings() {
  $form = array();

  $replacements = array();
  foreach (entity_get_info() as $entity_type => $entity_type_info) {
    $replacements['@entity_type'] = check_plain($entity_type_info['label']);
    $form["styleguide_view_mode_enable_{$entity_type}"] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow bundle selection for entity type @entity_type', $replacements),
      '#default_value' => variable_get("styleguide_view_mode_enable_{$entity_type}"),
    );
    $form["container_{$entity_type}"] = array(
      '#type' => 'fieldset',
      '#states' => array(
        'visible' => array(
          ':input[name="' . "styleguide_view_mode_enable_{$entity_type}" .'"]' => array('checked' => TRUE),
        ),
      ),
    );

    foreach ($entity_type_info['bundles'] as $bundle => $bundle_info) {
      $replacements['@bundle'] = check_plain($bundle_info['label']);
      $form["container_{$entity_type}"]["styleguide_view_mode_enable_{$entity_type}_{$bundle}"] = array(
        '#type' => 'checkbox',
        '#title' => t('Allow view mode selection for bundle @bundle', $replacements),
        '#default_value' => variable_get("styleguide_view_mode_enable_{$entity_type}_{$bundle}"),
      );
      $form["container_{$entity_type}"]["container_{$entity_type}_{$bundle}"] = array(
        '#type' => 'fieldset',
        '#states' => array(
          'visible' => array(
            ':input[name="' . "styleguide_view_mode_enable_{$entity_type}_{$bundle}" .'"]' => array('checked' => TRUE),
          ),
        ),
      );

      $view_modes = array();
      foreach ($entity_type_info['view modes'] as $view_mode => $view_mode_info) {
        $replacements['@view_mode'] = check_plain($view_mode_info['label']);
        $view_modes[$view_mode] = t('Enable view mode @view_mode', $replacements);
      }

      $form["container_{$entity_type}"]["container_{$entity_type}_{$bundle}"]["styleguide_view_mode_display_{$entity_type}_{$bundle}"] = array(
        '#type' => 'checkboxes',
        '#title' => t('Display view modes for entity @entity_type bundle @bundle', $replacements),
        '#options' => $view_modes,
        '#default_value' => variable_get("styleguide_view_mode_display_{$entity_type}_{$bundle}", array()),
      );
    }
  }

  return system_settings_form($form);
}
